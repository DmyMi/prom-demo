helm install example ./prom-app \
--set-file prometheus.extraScrapeConfigs=extraScrapeConfigs.yaml \
--set-file grafana.dashboards.default.my-dashboard.json=dashboardJson.json



# Grafana variable query
label_values(http_server_requests_seconds_sum{application="prom-app"}, outcome)
label_values(http_server_requests_seconds_sum{application="prom-app"}, uri)

# Grafana graph query
rate(http_server_requests_seconds_sum{application="prom-app", outcome="$outcome", uri="$uri"}[5m])

# Legend
access to {{ application }} with method: {{ method }}