FROM openjdk:8-alpine as buildcontainer
WORKDIR /app
COPY . /app
RUN ./gradlew bootJar

FROM java:8-alpine
WORKDIR /app
COPY --from=buildcontainer /app/build/libs/prom-demo-*.jar /app
EXPOSE 8080
CMD ["/bin/sh", "-c", "java -jar /app/prom-demo-*.jar"]
