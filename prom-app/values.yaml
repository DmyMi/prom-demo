# Default values for prom-app.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

replicaCount: 1

image:
  repository: registry.gitlab.com/dmymi/prom-demo
  pullPolicy: IfNotPresent

nameOverride: ""
fullnameOverride: ""

service:
  type: LoadBalancer
  port: 80

###### PROMETHEUS ######
prometheus:
  alertmanager:
    enabled: true
    persistentVolume:
      enabled: false

  server:
    global:
      ## How frequently to scrape targets by default
      ##
      scrape_interval: 1m
      ## How long until a scrape request times out
      ##
      scrape_timeout: 10s
      ## How frequently to evaluate rules
      ##
      evaluation_interval: 1m

    persistentVolume:
      ## If true, Prometheus server will create/use a Persistent Volume Claim
      ## If false, use emptyDir
      ##
      enabled: false

  ## alertmanager ConfigMap entries
  ##
  alertmanagerFiles:
    alertmanager.yml:
      global: {}
      # slack_api_url: ''

      receivers:
        - name: default-receiver
          # slack_configs:
          #  - channel: '@you'
          #    send_resolved: true

      route:
        group_wait: 10s
        group_interval: 5m
        receiver: default-receiver
        repeat_interval: 3h

  ## Prometheus server ConfigMap entries
  ##
  serverFiles:

    ## Alerts configuration
    ## Ref: https://prometheus.io/docs/prometheus/latest/configuration/alerting_rules/
    alerting_rules.yml: {}
    # groups:
    #   - name: Instances
    #     rules:
    #       - alert: InstanceDown
    #         expr: up == 0
    #         for: 5m
    #         labels:
    #           severity: page
    #         annotations:
    #           description: '{{ $labels.instance }} of job {{ $labels.job }} has been down for more than 5 minutes.'
    #           summary: 'Instance {{ $labels.instance }} down'
    ## DEPRECATED DEFAULT VALUE, unless explicitly naming your files, please use alerting_rules.yml
    alerts: {}

    ## Records configuration
    ## Ref: https://prometheus.io/docs/prometheus/latest/configuration/recording_rules/
    recording_rules.yml: {}
    ## DEPRECATED DEFAULT VALUE, unless explicitly naming your files, please use recording_rules.yml
    rules: {}

  # adds additional scrape configs to prometheus.yml
  # must be a string so you have to add a | after extraScrapeConfigs:
  # example adds prometheus-blackbox-exporter scrape config
  extraScrapeConfigs:
  # - job_name: 'prometheus-blackbox-exporter'
  #   metrics_path: /probe
  #   params:
  #     module: [http_2xx]
  #   static_configs:
  #     - targets:
  #       - https://example.com
  #   relabel_configs:
  #     - source_labels: [__address__]
  #       target_label: __param_target
  #     - source_labels: [__param_target]
  #       target_label: instance
  #     - target_label: __address__
  #       replacement: prometheus-blackbox-exporter:9115

  # Adds option to add alert_relabel_configs to avoid duplicate alerts in alertmanager
  # useful in H/A prometheus with different external labels but the same alerts
  alertRelabelConfigs:
  # alert_relabel_configs:
  # - source_labels: [dc]
  #   regex: (.+)\d+
  #   target_label: dc

###### GRAFANA ######
grafana:
  persistence:
    enabled: false

  # Administrator credentials when not using an existing secret (see below)
  adminUser: admin
  adminPassword: strongpassword

  ## Configure grafana datasources
  ## ref: http://docs.grafana.org/administration/provisioning/#datasources
  ##
  datasources:
    datasources.yaml:
      apiVersion: 1
      datasources:
      - name: Prometheus
        type: prometheus
        url: "http://{{ .Release.Name }}-prometheus-server"
        access: proxy
        isDefault: true

  dashboardProviders:
    dashboardproviders.yaml:
      apiVersion: 1
      providers:
      - name: 'default'
        orgId: 1
        folder: ''
        type: file
        disableDeletion: false
        editable: true
        options:
          path: /var/lib/grafana/dashboards/default
  ## Configure grafana dashboard to import
  ## NOTE: To use dashboards you must also enable/configure dashboardProviders
  ## ref: https://grafana.com/dashboards
  ##
  ## dashboards per provider, use provider name as key.
  ##
  dashboards: {}
    # default:
    #   some-dashboard:
    #     json: |
    #       $RAW_JSON
    #   my-dashboard:
    #     file: dashboards/dashboard-file.json
    #   prometheus-stats:
    #     gnetId: 2
    #     revision: 2
    #     datasource: Prometheus
    #   local-dashboard:
    #     url: https://example.com/repository/test.json
    #   local-dashboard-base64:
    #     url: https://example.com/repository/test-b64.json
  #     b64content: true
## Sidecars that collect the configmaps with specified label and stores the included files them into the respective folders
## Requires at least Grafana 5 to work and can't be used together with parameters dashboardProviders, datasources and dashboards
  sidecar:
    dashboards:
      enabled: true