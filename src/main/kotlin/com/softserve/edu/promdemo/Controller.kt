package com.softserve.edu.promdemo

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class Controller {
  
  @GetMapping("/")
  fun saySomething(): String {
    return "Index works, go to /actuator"
  }
}