package com.softserve.edu.promdemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PromDemoApplication

fun main(args: Array<String>) {
	runApplication<PromDemoApplication>(*args)
}
